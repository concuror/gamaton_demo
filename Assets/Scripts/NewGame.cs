using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

public class NewGame : MonoBehaviour {
	private Scene scene;
	private List<GameObject> monsterUnits;
	
	public GameObject endgame;	
	public GameObject background;
	public GameObject monsterPrefab;
	public GameObject towerMonsterPrefab;
	// Use this for initialization
	void Start () {
		scene = Scene.Instance;
		Cell[,] cells = scene.getCells();
		float xRat = Scene.XRatio();
		float yRat = Scene.YRatio();
		for (int i = 0; i < Scene.xmax; ++i) {
			for (int j = 0; j < Scene.ymax; ++j) {
				cells[i,j].center = new Vector3(xRat*i + xRat/2,yRat*j + yRat);
			}
		}
		StartCoroutine(startStage());
		StartCoroutine(doMove());
	}
	
	
	public void AddMonsters(List<Monster> monsters) {
		if (monsterUnits == null) {
			monsterUnits = new List<GameObject>(monsters.Count);
		}
		foreach (Monster monster in monsters) {
			GameObject monsterUnit;
			if(monster.monsterType == Monster.MonsterType.Castle)
				monsterUnit = (GameObject)Instantiate(monsterPrefab);
			else
				monsterUnit = (GameObject)Instantiate(towerMonsterPrefab);
			
			monsterUnit.transform.parent = background.transform;
			monsterUnit.GetComponent<MonsterBehaviour>().SetModel(monster);
			monsterUnits.Add(monsterUnit);
		}
	}
	
	public IEnumerator startStage(){
		while(true){
			int monsternum = 10;
			List<Monster> newMonsters = new List<Monster>();
			for (int i = 0;i < monsternum;i++){
				int rand = Random.Range(0, 5);
				Monster m;
				if(rand == 0)
					m = new TowerMonster();
				else
					m = new CastleMonster();
				m.hp = 30 + scene.currstage * 5;
				m.radius = 12;
				m.position = new Position(2,i * 25);
				
				newMonsters.Add(m);
			}
			scene.SpawnMonsters(newMonsters);
			AddMonsters(newMonsters);
			float waittime = 15.0f;
			scene.currstage++;
			yield return new WaitForSeconds(waittime);
		}
	}
	// Update is called once per frame
	void Update () {
		PlayerStats stats = PlayerStats.instance;
		if(stats.getLifes() <= 0){
			Time.timeScale = 0.0f;
			endgame.SetActive(true);
		}
	}
	
	private float max( float a, float b){
		return a > b ? a : b;
	}
	private IEnumerator doMove(){
		while(true){
			yield return new WaitForSeconds(max(1 / 20f - 1 / (400 * scene.currstage), 0.001f));
			scene.moveMonsters();
		}
	}
	
	public void Restart() {
		endgame.SetActive(false);
		scene.restart();
	}
}
