using System;
using System.Runtime.CompilerServices;

public class PlayerStats {

	public int lifes = 50;
	public int coins = 500;
	public int getLifes(){
		return lifes;
	}
	private static PlayerStats stats = new PlayerStats();
	public static PlayerStats instance{
		get{
			return stats;
		}
	}
	private PlayerStats (){
		
	}
	[MethodImpl(MethodImplOptions.Synchronized)]
	public void removeLife(){
		lifes--;	
	}
}


