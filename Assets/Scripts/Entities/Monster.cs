using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public abstract class Monster {
	
	public enum MonsterType {
		Castle,
		Tower
	}
	
	public int hp;
	public int defence;
	public int radius;
	public Position position;
	public Position lastPosition;
	public bool active = true;
	public MonsterType monsterType;
	
	abstract public void ApplyDamage(int damage);
	public bool isDead(){
		return hp <= 0;
	}
	
}
