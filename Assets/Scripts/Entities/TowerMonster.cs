using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;

public class TowerMonster : Monster {
	
	public AbstractTower destinationTower;
	public bool nearTower;
	

	override public void ApplyDamage(int damage){
		hp -= damage;	
	}
	
	public TowerMonster() {
		monsterType = Monster.MonsterType.Tower;
		hp = 120;
	}
	
	public void AttackTower() {
		destinationTower.AttackTower(1);
	}
	
}
