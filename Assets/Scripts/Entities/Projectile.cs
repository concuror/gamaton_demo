using UnityEngine;
using System.Collections;

public class Projectile {
	public int speed;
	public Position coordinate;
	public int damage;
	
	public Projectile(int speed, int damage) {
		this.speed = speed;
		this.damage = damage;
	}
	
}
