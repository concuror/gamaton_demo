using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
public class Scene {
	public static readonly int xmax = 320;
	public static readonly int ymax = 240;
	private static readonly int spawnx = 5;
	private static readonly int castlex = xmax - 5;
	public int currstage;
	private Cell[,] cells;
	public Cell[,] getCells(){
		return cells;
	}
	private HashSet<Monster> monsters;
	public HashSet<Monster> getMonsters(){
		return monsters;
	}
	private HashSet<AbstractTower> towers;
	public HashSet<AbstractTower> getTowers(){
		return towers;
	}

	private static readonly Scene instance = new Scene();
	private Scene(){
		cells = new Cell[xmax,ymax];	
		for(int i = 0;i<xmax;i++){
			for(int j = 0;j< ymax;j++){
				cells[i,j] = new Cell();
			}
		}
		monsters = new HashSet<Monster>();
		towers = new HashSet<AbstractTower>();
		currstage = 1;
	}
	public static Scene Instance{
		get{
			return instance;
		}
	}
	
	//min
	private int min(int a,int b){
		return a < b? a:b;
	}
	//max;
	private int max(int a,int b){
		return a > b? a:b;
	}

	
	public static readonly float sceneWidth = 1024;
	public static readonly float sceneHeight = 768;

	private static int abs(int x){
		return x < 0? -x : x;
	}
	public static int Distance(Position a, Position b) {
		return abs(a.x - b.x) + abs(a.y - b.y);
	}
	
	[MethodImpl(MethodImplOptions.Synchronized)]
	public void RemoveTower (AbstractTower tower) {
		Position pos = tower.position;
		int towerRadius = tower.radius;
		int x = pos.x, y = pos.y;
		//checking if we can place tower
		int startx = max(x - towerRadius,spawnx),
			maxx = min(x + towerRadius,castlex),
			starty = max(y - towerRadius,1),
			maxy = min(y + towerRadius,ymax-1);
		for(int i = startx;i < maxx;i++){
			for(int j = starty;j < maxy;j++){
				cells[i,j].hasTower = false;
				//if(cells[i,j].HasMonsters()) return false;
			}
		}
		towers.Remove(tower);
	}
	
	public bool PlaceTower(AbstractTower tower){
		Position pos = tower.position;
		int towerRadius = tower.radius;
		int x = pos.x, y = pos.y;
		//checking if we can place tower
		int startx = max(x - towerRadius,spawnx),
			maxx = min(x + towerRadius,castlex),
			starty = max(y - towerRadius,1),
			maxy = min(y + towerRadius,ymax-1);
		for(int i = startx;i < maxx;i++){
			for(int j = starty;j < maxy;j++){
				if(cells[i,j].hasTower) return false;
				//if(cells[i,j].HasMonsters()) return false;
			}
		}
		//can place
		for(int i = startx;i < maxx;i++){
			for(int j = starty;j < maxy;j++){
				if(Distance(new Position(i,j),pos) +1 <= towerRadius){
					cells[i,j].hasTower = true;			
				}
			}
		}
		towers.Add(tower);
		return true;
	}
	[MethodImpl(MethodImplOptions.Synchronized)]
	public void moveMonsters() {
		//monsters who got to the wall
		List<Monster> wonMonsters = new List<Monster>();
		foreach(Monster m in monsters) {
//			Debug.Log(m.position);
			Position p;
			if(m.monsterType == Monster.MonsterType.Castle)
				p = MovingTactic.MoveTo(m,cells);
			else
				p = MovingTactic.MoveToNearTower((TowerMonster)m, towers, cells);
			//int x = p.x, y = p.y;
			//cells[m.position.x,m.position.y].RemoveMonster(m);
			m.lastPosition = m.position;
			m.position = p;
			//cells[x,y].AddMonster(m);	
			if(m.position.x >= castlex){
				wonMonsters.Add(m);
			}
		}
		foreach(Monster m in wonMonsters){
			//Debug.Log("removed won");
			m.active = false;
			monsters.Remove(m);
			//reduce player life
			PlayerStats.instance.removeLife();
		}
	}
	[MethodImpl(MethodImplOptions.Synchronized)]
	public void damage(Position p,int damage, int radius){
		List<Monster> deadMonsters = new List<Monster>();
		foreach(Monster m in monsters){
			if(m.radius + radius >= Distance(m.position,p)){
				//F..k, we got hit :(
				m.ApplyDamage(damage);
				if(m.isDead()){ deadMonsters.Add(m);}
				//damage for moving algo;
				int r = 30;
				int x = p.x;
				int y = p.y;
				int startx = max(x - r,spawnx),
					maxx = min(x + r,castlex),
					starty = max(y - r,0),
					maxy = min(y + r,ymax);
				for(int i = startx;i < maxx;i++) {
					for(int j = starty;j < maxy;j++){
						//Debug.Log("i: " + i + " ;j: " + j);
						int dist = Distance(new Position(i,j),p);
						cells[i,j].UpdateWithDamage(Mathf.Max(r - dist, 0));	
					}
				}
			}			
		}
		foreach(Monster m in deadMonsters){
			m.active = false;
			monsters.Remove(m);
		}
	}
	
	static public float XRatio() {
		return sceneWidth / xmax;
	}
	
	static public float YRatio() {
		return sceneHeight / ymax;
	}
	
	
	public static Position PositionFromCoordinate(Vector3 coord) {
		float i = coord.x / XRatio();
		float j = coord.y / YRatio();
		return new Position((int)i, (int)j);
	}
	
	public Cell CellFromCoordinate(Vector3 coord) {
		Position pos = PositionFromCoordinate(coord);
		return cells[pos.x,pos.y];
	}
	[MethodImpl(MethodImplOptions.Synchronized)]
	public void SpawnMonsters(List<Monster> newMonsters){
		//Debug.Log(newMonsters);
		foreach(Monster m in newMonsters){
			monsters.Add(m);
			//int x = m.position.x, y = m.position.y;
			//cells[x,y].AddMonster(m);
		}
	}
	[MethodImpl(MethodImplOptions.Synchronized)]
	public void restart(){
		foreach(Monster m in monsters){
			m.active = false;
		}	
		monsters.Clear();
		towers.Clear();
		currstage = 1;
		
	}
}
