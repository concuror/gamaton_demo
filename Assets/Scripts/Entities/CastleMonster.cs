using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;
public class CastleMonster : Monster {
	
	override public void ApplyDamage(int damage){
		hp -= damage;	
	}
	
	public CastleMonster() {
		monsterType = Monster.MonsterType.Castle;
		hp = 30;
	}
}
