using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Cell {
	
	static float dangerFallRatio = 1f;//0.98f;
	static float flavourFallRatio = 0.92f;
	
	public bool hasTower;
	//public ArrayList monsters;
	
	public float flavour;
	public float danger;
	// time when was last update of monster info
	public float lastInfoUpdate;
	public Vector3 center;
	
	public Scene scene;
	
	/*[MethodImpl(MethodImplOptions.Synchronized)]
	public void RemoveMonster(Monster monster) {
		if(monsters.Contains(monster)){
			monsters.Remove(monster);	
		};
	}
	
	[MethodImpl(MethodImplOptions.Synchronized)]
	public void AddMonster(Monster monster) {
		if(!monsters.Contains(monster)){
			monsters.Add(monster);
		}
	}
	
	public bool HasMonsters(){
		return (monsters.Count > 0);
	}*/
	
	public Cell() {
		//monsters = new ArrayList();		
		flavour = 0f;
		danger = 0f;
		lastInfoUpdate = Time.time;
	}
	
	public void UpdateAttraction () {
		UpdateAttraction(0f, 0f);
	}
	
	public void UpdateAttraction (float newDamage, float newFlavour) {
		float t = Time.time;
		flavour = flavour*Mathf.Pow(flavourFallRatio, t - lastInfoUpdate) + newFlavour;
		danger = danger*Mathf.Pow(dangerFallRatio, t - lastInfoUpdate) + newDamage;
		lastInfoUpdate = t;
	}	
	
	public void UpdateWithDamage (float damage) {
		UpdateAttraction(damage, 0f);
	}
	
	public void UpdateWithFlavour (float flavour) {
		UpdateAttraction(0f, flavour);
	}
}
