using System;

public struct Position{
	public int x;
	public int y;
	
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public override String ToString(){
		return x + " " + y;
	}
}


