using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class MovingTactic{
	
	static Position[] directions = {new Position(0,1), new Position(1,1), new Position(1,0), new Position(1,-1), new Position(0,-1)};
	
	static float primaryDanger = 10f;
	static float primaryFlavour = 10f;
	
	private static bool CanMove(Position pos, Cell[,] cells, Monster monster){
		if(pos.x < 0 || pos.x >= Scene.xmax || pos.y < 0 || pos.y >= Scene.ymax)
			return false;
		if(monster.lastPosition.x == pos.x && monster.lastPosition.y == pos.y)
			return false;
		Cell cell = cells[pos.x, pos.y];
		return !cell.hasTower;
	}
	
	private static float CalculateAttraction (Cell cell) {
		cell.UpdateAttraction();
		return (cell.flavour + primaryFlavour) / (cell.danger + primaryDanger);
	}
	
	private static void UpdateCellsWithRadius (Position pos, int radius, Cell[,] cells) {
		int minx = Mathf.Max(0, pos.x - radius),
			maxx = Mathf.Min(Scene.xmax-1, pos.x + radius),
			miny = Mathf.Max (0, pos.y - radius),
			maxy = Mathf.Min (Scene.ymax-1, pos.y+radius);
		for(int i = minx; i <= maxx; i++) {
			for(int j = miny; j <= maxy; j++) {
				cells[i, j].UpdateWithFlavour(1f);
			}
		}
	}
	
	private static int StepsDistance(Position a, Position b) {
		return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
	}
	
	public static Position MoveToNearTower(TowerMonster monster, HashSet<AbstractTower> towers, Cell[,] cells) {
		Position pos = monster.position;
		
		Position destPos;
		if(monster.destinationTower != null)
			destPos = monster.destinationTower.position;
		else
			destPos = new Position(0, 0);
		
		if(monster.nearTower && cells[destPos.x, destPos.y].hasTower) {
			monster.AttackTower();
			return pos;
		}
		
		if(monster.destinationTower != null && cells[destPos.x, destPos.y].hasTower == false 
			|| monster.destinationTower == null) {
			
			monster.nearTower = false;
			
			if(monster.destinationTower != null) {
				monster.destinationTower = null;
			}
			
			int minDist = 2 * (Scene.xmax - pos.x);
			foreach(AbstractTower tower in towers) {
				if(Scene.Distance(pos, tower.position) < minDist && tower.position.x > pos.x) {
					minDist = Scene.Distance(pos, tower.position);
					monster.destinationTower = tower;
				}
			}
		}
		
		Position destinationPoint;
		int radius;
		if(monster.destinationTower == null) {
			destinationPoint = new Position(Scene.xmax, pos.y);
			radius = 0;
		}
		else {
			destinationPoint = monster.destinationTower.position;
			radius = monster.destinationTower.radius;
		}
		
		if(Scene.Distance(pos, destinationPoint) < radius + 2) {
			monster.nearTower = true;
			return pos;
		}
		
		int[] distances = new int[5];
		int minD = 2 * Scene.xmax; 
		int minNum = 0;
		
		for(int i = 0; i < 5; i++) {
			Position newPos = new Position(pos.x + directions[i].x, pos.y + directions[i].y);
			if(newPos.y >= 0 && newPos.y < Scene.ymax && newPos.x >= 0 && newPos.x < Scene.xmax) {
				distances[i] = MovingTactic.StepsDistance(newPos, destinationPoint);
				if(cells[newPos.x, newPos.y].hasTower)
					distances[i] = 2 * Scene.xmax;
				if(distances[i] <= minD) {
					minD = distances[i];
					minNum = i;
				}
			}
		}
		
		return new Position(pos.x + directions[minNum].x, pos.y + directions[minNum].y);
		
	}
	
	public static Position MoveTo(Monster monster, Cell[,] cells){
		Position pos = monster.position;
		float[] probabilities = new float[5];
		float totalProbability = 0f;
		
		// if no danger forward then go
		if(cells[pos.x + directions[2].x, pos.y + directions[2].y].danger < 0.01f && 
			CanMove(new Position(pos.x + directions[2].x, pos.y + directions[2].y), cells, monster))
		{
			UpdateCellsWithRadius(new Position(pos.x + directions[2].x, pos.y + directions[2].y), 5, cells);
			return new Position(pos.x + directions[2].x, pos.y + directions[2].y);
		}
		
		for(int i = 0; i < 5; i++) {
			if(MovingTactic.CanMove(new Position(pos.x + directions[i].x, pos.y + directions[i].y), cells, monster)) {
				probabilities[i] = CalculateAttraction(cells[pos.x + directions[i].x, pos.y + directions[i].y]);
			}
			else {
				probabilities[i] = 0f;
			}
			totalProbability += probabilities[i];
		}
		if(totalProbability == 0f)
			return pos;
		for(int i = 0; i < 5; i++) {
			probabilities[i] /= totalProbability;
		}
		
		float rand = Random.Range(0, 1000) / 1000f;
		totalProbability = 0f;
		
		for(int i = 0; i < 5; i++) {
			totalProbability += probabilities[i];
			if(rand < totalProbability) {
				UpdateCellsWithRadius(new Position(pos.x + directions[i].x, pos.y + directions[i].y), 5, cells);
				return new Position(pos.x + directions[i].x, pos.y + directions[i].y);
			}
		}
		
		
		return pos;
	}
}
