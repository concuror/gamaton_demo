using UnityEngine;
using System.Collections;

public class LoadinScript : MonoBehaviour {
	public exSpriteAnimation loadAnimation;
	void Start () {
		StartCoroutine(animate());
	}
	private IEnumerator animate(){
		yield return new WaitForSeconds(0.5f);
		loadAnimation.PlayDefault();
		yield return new WaitForSeconds(6.5f);
		Application.LoadLevelAsync("Main");
	}

}
