using UnityEngine;
using System.Collections;

public class ElectricSparkBehaviour : MonoBehaviour {
	
	Projectile model;
	
	Position startPosition;
	
	public void SetModel(Projectile model, Position startPosition, Monster targetMonster) {
		this.model = model;
		this.model.coordinate = startPosition;
		Position endPosition = targetMonster.position;
		Cell endCell = Scene.Instance.getCells()[endPosition.x, endPosition.y];
		float distance = Vector3.Distance(transform.localPosition, endCell.center);
		float trueSpeed = Scene.XRatio() * model.speed;
		float time = distance / trueSpeed;
		TweenPosition.Begin(gameObject, time, endCell.center + new Vector3(0,0,-2));
		StartCoroutine(Damage(targetMonster, time));
		Destroy(gameObject, time+0.01f);
	}
	
	public IEnumerator Damage(Monster targetMonster, float time) {
		yield return new WaitForSeconds(time);
		Scene.Instance.damage(targetMonster.position,model.damage,1);
	}
	
}
