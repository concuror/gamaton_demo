using UnityEngine;
using System.Collections;

public class BombBehaviour : MonoBehaviour {
	
	public Position endPosition;
	public Projectile model;
	public GameObject embersPrefab;
	public int radius;
	
	public void SetModel(Projectile model, Position endPosition, int radius) {
		this.radius = radius;
		this.model = model;
		this.model.coordinate = Scene.PositionFromCoordinate(transform.localPosition);
		this.endPosition = endPosition;
		Cell endCell = Scene.Instance.getCells()[endPosition.x, endPosition.y];
		float distance = Vector3.Distance(transform.localPosition, endCell.center);
		float trueSpeed = Scene.XRatio() * model.speed;
		float time = distance / trueSpeed;
		gameObject.GetComponent<TweenScale>().duration = time;
		TweenPosition.Begin(gameObject, time, endCell.center + new Vector3(0,0,-2));
		Invoke("Expload",time);
	}
	
	public void Expload() {
		Scene.Instance.damage(endPosition, model.damage, radius);
		GameObject embers = (GameObject)Instantiate(embersPrefab);
		embers.transform.parent = transform.parent;
		embers.transform.localPosition = transform.localPosition + new Vector3(0,0,1);
		Destroy(embers, 1.05f);
		Destroy(gameObject);
	}
}
