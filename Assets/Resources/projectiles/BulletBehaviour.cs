using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletBehaviour : MonoBehaviour {
	
	Projectile model;
	Vector2 direction;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Position bullPos = Scene.PositionFromCoordinate(gameObject.transform.localPosition);
		if ((bullPos.x == Scene.xmax - 1) || (bullPos.y == Scene.ymax - 1) || (bullPos.x == 0) || (bullPos.y == 0)) {
			Destroy(gameObject);
			return;
		}
		Scene.Instance.damage(bullPos,model.damage,0);
	}
	
	public void SetModel(Projectile model, Vector2 direction) {
		this.model = model;
		this.model.coordinate = Scene.PositionFromCoordinate(transform.localPosition);
		Vector2 start = new Vector2(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y);
		Vector2 res = VectorRectangleIntersect(start, direction, new Position(0,0), new Position(1024,768));
		Vector3 endPosition = new Vector3(res.x, res.y, -4);
		float dist = Vector3.Distance(gameObject.transform.localPosition, endPosition);
		float trueSpeed = Scene.XRatio() * model.speed;
		TweenPosition.Begin(gameObject, dist/trueSpeed, endPosition);
	}
	
	private Vector2 VectorRectangleIntersect (Vector2 start, Vector2 vect, Position leftBottomSide, Position rightUpSide) {
		
		float leftDist = 10000f;
		float rightDist = 10000f;
		float upDist = 10000f;
		float bottomDist = 10000f;
		
		Debug.Log(start + " " + vect);
		
		if(vect.x < 0) {
			leftDist = (-start.x + leftBottomSide.x) / (vect.x / vect.magnitude);
		}
		else if(vect.x > 0) {
			rightDist = (rightUpSide.x - start.x) / (vect.x / vect.magnitude);
		}
		else if(vect.y < 0) {
			bottomDist = (start.y - leftBottomSide.y) / (vect.y / vect.magnitude);
		}
		else if(vect.y > 0) {
			rightDist = (rightUpSide.x - start.y) / (vect.y / vect.magnitude);
		}
		
		if(leftDist <= rightDist && leftDist <= upDist && leftDist <= bottomDist) {
			return new Vector2(start.x + vect.x * (leftDist / vect.magnitude), start.y + vect.y * (leftDist / vect.magnitude));
		}
		else if(rightDist <= leftDist && rightDist <= upDist && rightDist <= bottomDist) {
			return new Vector2(start.x + vect.x * (rightDist / vect.magnitude), start.y + vect.y * (rightDist / vect.magnitude));
		}
		else if(bottomDist <= leftDist && bottomDist <= upDist && bottomDist <= rightDist) {
			return new Vector2(start.x + vect.x * (bottomDist / vect.magnitude), start.y + vect.y * (bottomDist / vect.magnitude));
		}
		else {
			return new Vector2(start.x + vect.x * (upDist / vect.magnitude), start.y + vect.y * (upDist / vect.magnitude));
		}
			
	}
}
