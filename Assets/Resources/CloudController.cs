using UnityEngine;
using System.Collections;

public class CloudController : MonoBehaviour {

	static float moveTime = 25;
	
	void Start () {
		RestartCloud();
	}
	
	void RestartCloud(){
		
		gameObject.transform.localPosition = new Vector3(1300, Random.Range(40, 750), -7);
		TweenPosition.Begin(gameObject, moveTime, new Vector3(-300, transform.localPosition.y - 50, -7));
		
		Invoke("RestartCloud", moveTime);
	}
	
}
