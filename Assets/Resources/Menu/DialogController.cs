using UnityEngine;
using System.Collections;

public class DialogController : MonoBehaviour {
	
	public UILabel monsterDialog;
	public UISprite monsterDialogBackground;
	
	public UILabel castleDialog;
	public UISprite castleDialogBackground;
	
	private AudioSource audioSource;
	public AudioClip telegraph1;
	public AudioClip telegraph2_2;
	public AudioClip telegraph3;
	public AudioClip telegraph3_2;
	public AudioClip telegraph6;
	
	public AudioClip music;
	public GameObject panel;
	
	private string[] castleText1; 
	private AudioClip[] castleSound1;
	
	private string[] monsterText1;
	private AudioClip[] monsterSound1;
	
	private string[] castleText2; 
	private AudioClip[] castleSound2;
	
	private string[] monsterText2;
	private AudioClip[] monsterSound2;
	
	
	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource>();
		
		monsterDialogBackground.gameObject.SetActive(false);
		monsterDialog.text = "";
		castleDialog.text = "";
		
		
		
		castleText1 = new string[]{"Ey?", " Yo, man,", "\nwhat are", "\nyou doing", "\nthere?"};
		castleSound1 = new AudioClip[]{telegraph2_2, telegraph3, telegraph3_2, telegraph3_2, telegraph1};
		
		monsterText1 = new string[]{"What&?", "\nYou have", "\na problem?"};
		monsterSound1 = new AudioClip[]{telegraph1, telegraph3, telegraph3};
		
		castleText2 = new string[]{"You", "\nare my", "\nproblem!"};
		castleSound2 = new AudioClip[]{telegraph3, telegraph3_2, telegraph6};
		
		monsterText2 = new string[]{"**x* *f*", "\n****!", "\nI'll kick", "\nyour ass!"};
		monsterSound2 = new AudioClip[]{telegraph3_2, telegraph3, telegraph6, telegraph1};
		
		StartCoroutine("PlayDialog");
	}
	
	
    public static IEnumerator WaitForRealSeconds(float seconds) {
        var ms = seconds * 1000.0f;
        var stopwatch = new System.Diagnostics.Stopwatch();
 
        stopwatch.Start();
 
        while (stopwatch.ElapsedMilliseconds < ms) {
            yield return null;
        }
    }
	
	IEnumerator PlayDialog(){
		Time.timeScale = 0.0f;
		
		for(int i=0; i<castleText1.Length; i++){
			
			castleDialog.text = castleDialog.text+castleText1[i];
			audioSource.PlayOneShot(castleSound1[i], 1);
			
			yield return StartCoroutine(WaitForRealSeconds(0.7f));
		}
		
		monsterDialogBackground.gameObject.SetActive(true);
		for(int i=0; i<monsterText1.Length; i++){
			
			monsterDialog.text = monsterDialog.text+monsterText1[i];
			audioSource.PlayOneShot(monsterSound1[i], 1);
			
			yield return StartCoroutine(WaitForRealSeconds(0.7f));
		}
		
		castleDialog.text = "";
		for(int i=0; i<castleText2.Length; i++){
			
			castleDialog.text = castleDialog.text+castleText2[i];
			audioSource.PlayOneShot(castleSound2[i], 1);
			
			yield return StartCoroutine(WaitForRealSeconds(0.7f));
		}
		
		monsterDialog.text = "";
		for(int i=0; i<monsterText2.Length; i++){
			
			monsterDialog.text = monsterDialog.text+monsterText2[i];
			audioSource.PlayOneShot(monsterSound2[i], 1);
			
			yield return StartCoroutine(WaitForRealSeconds(0.7f));
		}
		
		audioSource.clip = music;
		audioSource.Play();
		Time.timeScale = 1.0f;
		monsterDialog.gameObject.SetActive(false);
		castleDialog.gameObject.SetActive(false);
		monsterDialogBackground.gameObject.SetActive(false);
		castleDialogBackground.gameObject.SetActive(false);
	}
	
	
}
