using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class BuilderBehaviour : MonoBehaviour {
	
	public TowerType currentType;
	
	public GameObject bombTower;
	
	public GameObject electricTower;
	
	public GameObject minigunTower;
	
	public TapGesture tapGesture;
	
	private Vector2 startTouchPos;
	
	public float buildTime;
	
	public exClipping clippingPlane;
	
	private float builtTime;

	// Use this for initialization
	void Start () {
		builtTime = -buildTime;
		tapGesture.StateChanged += OnTap;
	}
	
	void Update() {
		clippingPlane.height = ((Time.time - builtTime) / buildTime * 110);
	}
	
	public void ChangeType(TowerType type) {
		currentType = type;
	}
	
	private void OnTap(object sender, TouchScript.Events.GestureStateChangeEventArgs e){
		if ( e.State == TouchScript.Gestures.Gesture.GestureState.Recognized ) {
			if ((Time.time - builtTime) < buildTime) return;
			GameObject newTower;
			switch (currentType) {
			case TowerType.Bomb :
				newTower = (GameObject)Instantiate(bombTower);
				break;
			case TowerType.Electric:
				newTower = (GameObject)Instantiate(electricTower);
				break;
			case TowerType.Bullet:
				newTower = (GameObject)Instantiate(minigunTower);
				break;
			default:
				newTower = null;
				return;
			}
			newTower.transform.parent = gameObject.transform.parent;
			newTower.transform.localPosition = gameObject.transform.localPosition + new Vector3(-25.0f, 0.0f, -2.0f);
			builtTime = Time.time;
		}
	}
	/*
	private void OnFlick(object sender, TouchScript.Events.GestureStateChangeEventArgs e) {
		if ( e.State == TouchScript.Gestures.Gesture.GestureState.Recognized ) {
//			Vector2 vector = flickGesture.ScreenFlickVector;
			int modifier = ((vector.x > 0) || (vector.y > 0))?-1:1;
			int i = System.Math.Abs((int)currentType + modifier) % 2;
			currentType = (TowerType)i;
		}
	}
	*/
}
