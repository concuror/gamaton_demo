using UnityEngine;
using System.Collections;

public class MonsterExplousion : MonoBehaviour {

	public AudioClip destroySound;
	private AudioSource audioSource;
	
	void Awake () {
		audioSource = GetComponent<AudioSource>();
	}
	
	void Start (){
		audioSource.PlayOneShot(destroySound, 1.0f);
	}
	
}
