using UnityEngine;
using System.Collections;

public class MonsterBehaviour : MonoBehaviour {

	public Monster model;
	
	public GameObject deathPrefab;
	
	public void SetModel(Monster model) {
		this.model = model;
	}
	
	void Update() {
		if (model.active) {
			Position mPos = model.position;
			transform.localPosition = ((Cell)Scene.Instance.getCells()[mPos.x, mPos.y]).center + new Vector3(0,0,-1);
		} else {
			GameObject death = (GameObject)Instantiate(deathPrefab);
			death.transform.parent = transform.parent;
			death.transform.localPosition = transform.localPosition;
			Destroy(gameObject);
		}
	}
	
}
