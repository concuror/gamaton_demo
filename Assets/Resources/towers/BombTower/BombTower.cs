using UnityEngine;
using System.Collections;


public class BombTower : AbstractTower {
	
	public override int life{
		get{
			return 100 + 20 * level;
		}
	}
	public override int damage{
		get{
			return 25 + 10*level;
		}
	}
	public override float rechargeTime{
		get{
			return 1f;
		}
	}
	public override int radius{
		get{
			return 15;
		}
	}
	
	public BombTower () {
		this.currentLife = 2*(100 + 20 * level);
	}
}