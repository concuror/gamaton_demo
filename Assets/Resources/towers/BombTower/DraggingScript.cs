using UnityEngine;
using System.Collections;
using TouchScript;
using TouchScript.Gestures;

public class DraggingScript : MonoBehaviour {
	
	public GameObject touchBeganTarget;
	public string touchBeganFunction;
	public GameObject touchChangeTarget;
	public string touchChangeFunction;
	public GameObject touchEndTarget;
	public string touchEndFunction;
	
	private PanGesture panGesture;
	
	private Vector3 startPosition;
	private Vector2 startTouchPos;
	
	exSprite towerSprite;

	void Awake (){
		
		panGesture = GetComponent<PanGesture>();
			
	}
	
	void Start () {
		startPosition = Vector3.zero;
		panGesture.StateChanged += OnMove;
		towerSprite = gameObject.GetComponent<exSprite>();
	}
	
	public void OnMove(object sender, TouchScript.Events.GestureStateChangeEventArgs e){
		if ( e.State == TouchScript.Gestures.Gesture.GestureState.Changed ) {
			if (startPosition == Vector3.zero) {
				startPosition = gameObject.transform.localPosition;
				startTouchPos = panGesture.ActiveTouches[0].Position;
				
				
				if (towerSprite != null) {
					Vector2 scaleV = new Vector2(1.2f, 1.2f);
					towerSprite.scale = scaleV;
				}
				
				if(touchBeganTarget != null && !string.IsNullOrEmpty(touchBeganFunction)){
					touchBeganTarget.SendMessage(touchBeganFunction, gameObject, SendMessageOptions.DontRequireReceiver);
				}
				
			}
			Vector2 pos = panGesture.ActiveTouches[0].Position;
			Vector2 delta = pos-startTouchPos;
			gameObject.transform.localPosition = startPosition + new Vector3(delta.x, delta.y);
		
			if(touchChangeTarget != null && !string.IsNullOrEmpty(touchChangeFunction)){
				touchChangeTarget.SendMessage(touchChangeFunction, gameObject, SendMessageOptions.DontRequireReceiver);
			}
		}
		else if (e.State == TouchScript.Gestures.Gesture.GestureState.Ended) {
			startPosition = Vector3.zero;
			startTouchPos = Vector2.zero;
			if (towerSprite != null) {
					Vector2 scaleV = new Vector2(1, 1);
					towerSprite.scale = scaleV;
				}
			if(touchEndTarget != null && !string.IsNullOrEmpty(touchEndFunction)){
				touchEndTarget.SendMessage(touchEndFunction, gameObject, SendMessageOptions.RequireReceiver);
			}
		}
	}
	
}
