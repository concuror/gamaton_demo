using UnityEngine;
using System.Collections;

public class BombTowerBehaviour : ProjectileTowerBehaviour {
	
	//public BombTower model;
	public GameObject targetPrefab;
	public GameObject BombProjectile;
	
	private AudioSource audioSource;
	public AudioClip shootSound;
	
	private GameObject targetObject;
	private float fired;
	private bool isSetted = false;
	
	
	void Awake (){

		audioSource = GetComponent<AudioSource>();		
		model = new BombTower();

		model.position = Scene.PositionFromCoordinate(transform.localPosition);
		model.aimedPosition = new Position(50,0);
		fired = float.MaxValue;
	}
	
	void Start () {
		AddHighlight();
	}
	
	public override void Update() {
		base.Update();
		if ( (Time.time - fired) > model.rechargeTime ) {
			Shoot();
		}
	}

	private void Shoot() {
		audioSource.PlayOneShot(shootSound, 1f);
		
		fired = Time.time;
		GameObject bombObject = (GameObject)Instantiate(BombProjectile);
		bombObject.transform.parent = gameObject.transform.parent;
		bombObject.transform.localPosition = transform.localPosition + new Vector3(0,0,-2);
		BombBehaviour bomb = bombObject.GetComponent<BombBehaviour>();
		Projectile bombModel = new Projectile(200, model.damage);
		bomb.SetModel(bombModel,model.aimedPosition, 10);
	}
	
	
	public void TowerSetted(){
		model.position = Scene.PositionFromCoordinate(transform.localPosition);
		bool isSet = Scene.Instance.PlaceTower(model);
		
		if (isSet) {
			GetComponent<BoxCollider>().enabled = false;
			targetObject = (GameObject)Instantiate(targetPrefab);
			targetObject.transform.parent = gameObject.transform.parent;
			targetObject.transform.localPosition = gameObject.transform.localPosition + new Vector3(0,0,-2);
			targetObject.GetComponent<TargetController>().Init(this);
			RemoveHighlight();
		} else {
			Destroy(gameObject);
		}
	}
	
	override public void SetAimPosition(Vector3 position){
//		Debug.Log("Aim position "+position);
		model.aimedPosition = Scene.PositionFromCoordinate(position);
		
		if(!isSetted){
			isSetted = true;
			Shoot();
		}
	}
	
}
