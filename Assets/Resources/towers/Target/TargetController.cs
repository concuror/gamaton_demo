using UnityEngine;
using System.Collections;

public class TargetController : MonoBehaviour {
	
	public GameObject dashPrefab;
	private ArrayList dashes = new ArrayList();
	
	private Vector3 startPosition;
	private ProjectileTowerBehaviour bombTower;
	
	public void Init(ProjectileTowerBehaviour bombTower){
		this.bombTower = bombTower;
		startPosition = transform.localPosition;
		//GetComponent<exSprite>().enabled = false;
	}
	
	public void SendAimPosition(){
		bombTower.SetAimPosition(gameObject.transform.localPosition);
		//TweenScale.Begin(gameObject, 0.15f, new Vector3(1.1f, 1.1f, 1.1f));
		Invoke("EndSequence",0.2f);
		
		foreach(GameObject dash in dashes){
			Destroy(dash);
		}
		dashes.Clear();
		
	}
	
	public void EndSequence() {
		transform.localPosition = startPosition;
		GetComponent<exSprite>().enabled = false;
	}
	
	public void SetVisible(){
		GetComponent<exSprite>().enabled = true;
	}
	
	
	public void TargetMoved(){
		float dist = (transform.localPosition - startPosition).magnitude;
		int dashCount = (int)(dist/70);
		
		while(dashes.Count > dashCount){
			Destroy((GameObject)dashes[dashes.Count-1]);
			dashes.RemoveAt(dashes.Count-1);
		}
		
		while(dashes.Count < dashCount){
			GameObject dash = (GameObject)Instantiate(dashPrefab);
			dash.transform.parent = gameObject.transform.parent;
			dashes.Add(dash);
		}
		
		Vector3 direction = (transform.localPosition - startPosition).normalized * dist/dashCount;
		float angle = Mathf.Atan2(direction.x, direction.y) + Mathf.PI/2;
		int i=1;
		foreach(GameObject dash in dashes){
			dash.transform.localPosition = startPosition + (direction * i);
			dash.transform.eulerAngles = new Vector3(0, 0, -angle/Mathf.PI*180f);
			i++;
		}
		
		
	}
	
}
