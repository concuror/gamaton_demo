using System;

public class MinigunTower : AbstractTower {
	public override int life{
		get{
			return 80 + 12 * level;
		}
	}
	public override int damage{
		get{
			return 5 + 2 * level;
		}
	}
	public override float rechargeTime{
		get{
			return 0.33f;
		}
	}
	public override int radius{
		get{
			return 43;
		}
	}
	
	public MinigunTower () {
		this.currentLife = 2*(80 + 12 * level);
	}
}

