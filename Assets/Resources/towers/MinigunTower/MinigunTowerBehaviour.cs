using UnityEngine;
using System.Collections;

public class MinigunTowerBehaviour : ProjectileTowerBehaviour {
	
	//public MinigunTower model;
	public GameObject targetPrefab;
	public GameObject BulletProjectile;
	
	private AudioSource audioSource;
	public AudioClip shootSound;
	
	private GameObject targetObject;
	private float fired;
	private bool isSetted = false;
	
	
	void Awake (){

		audioSource = GetComponent<AudioSource>();		
		model = new MinigunTower();

		model.position = Scene.PositionFromCoordinate(transform.localPosition);
		model.aimedPosition = new Position(50,0);
		fired = float.MaxValue;
	}
	
	void Start () {
		AddHighlight();
	}
	
	public override void Update() {
		base.Update();
		if ( (Time.time - fired) > model.rechargeTime ) {
			Shoot();
		}
	}

	private void Shoot() {
		audioSource.PlayOneShot(shootSound, 1f);
		
		fired = Time.time;
		GameObject bulletObject = (GameObject)Instantiate(BulletProjectile);
		bulletObject.transform.parent = gameObject.transform.parent;
		bulletObject.transform.localPosition = transform.localPosition + new Vector3(0,0,-2);
		BulletBehaviour bullet = bulletObject.GetComponent<BulletBehaviour>();
		Projectile bulletModel = new Projectile(290, model.damage);
		Cell[,] cells = Scene.Instance.getCells();
		Vector3 res = cells[model.aimedPosition.x,model.aimedPosition.y].center - bulletObject.transform.localPosition;
		Vector2 direction = new Vector2(res.x, res.y);
		bullet.SetModel(bulletModel, direction);
	}
	
	
	public void TowerSetted(){
		model.position = Scene.PositionFromCoordinate(transform.localPosition);
		bool isSet = Scene.Instance.PlaceTower(model);
		
		if (isSet) {
			GetComponent<BoxCollider>().enabled = false;
			targetObject = (GameObject)Instantiate(targetPrefab);
			targetObject.transform.parent = gameObject.transform.parent;
			targetObject.transform.localPosition = gameObject.transform.localPosition + new Vector3(0,0,-2);
			targetObject.GetComponent<TargetController>().Init(this);
			RemoveHighlight();
		} else {
			Destroy(gameObject);
		}
	}
	
	override public void SetAimPosition(Vector3 position){
		model.aimedPosition = Scene.PositionFromCoordinate(position);
		
		if(!isSetted){
			isSetted = true;
			Shoot();
		}
	}
	
}
