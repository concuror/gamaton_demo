using UnityEngine;
using System.Collections;

public class AbstractTowerBehaviour : MonoBehaviour {
	
	
	public AbstractTower model;
	public GameObject highlightPrefab;
	private GameObject highlight;
	
	public virtual void Update() {
		if (model.isDead) {
			GameObject deth = (GameObject)Instantiate(Resources.Load("towers/_Textures/TowerDestruction"));
			deth.transform.parent = transform.parent;
			deth.transform.localPosition = transform.localPosition;
			Scene.Instance.RemoveTower(model);
			Destroy(gameObject);
		}
	}
	
	protected void AddHighlight(){
		highlight = (GameObject)Instantiate(highlightPrefab);
		highlight.transform.parent = gameObject.transform;
		highlight.transform.localPosition = new Vector3(0, 0, 0.5f);
	}
	
	protected void RemoveHighlight(){
		Destroy(highlight);
	}
	
	
}
