using UnityEngine;
using System.Collections;

public enum TowerType{
	Bomb,
	Electric,
	Bullet,
	Hammer
}
