using UnityEngine;
using System.Collections;


public class ElectricTower : AbstractTower {
	
	public override int life{
		get{
			return 70 + 12 * level;
		}
	}
	public override int damage{
		get{
			return 20 + 7 * level;
		}
	}
	public override float rechargeTime{
		get{
			return 1.8f;
		}
	}
	public override int radius{
		get{
			return 25;
		}
	}
	
	public ElectricTower () {
		this.currentLife = 2*(70 + 12 * level);
	}
}
