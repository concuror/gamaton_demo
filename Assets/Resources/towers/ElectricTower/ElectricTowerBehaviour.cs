using UnityEngine;
using System.Collections;

public class ElectricTowerBehaviour : AbstractTowerBehaviour {
	
	//public ElectricTower model;	
	public GameObject electricProjectile;
	
	public Position position;
	
	private float fired;
	private bool isSetted;
	
	private static int attackRadius = 55;
	private static float chainDelay = 0.5f;
	
	private AudioSource audioSource;
	public AudioClip shootSound;
	
	void Awake (){
		model = new ElectricTower();
		position = Scene.PositionFromCoordinate(transform.localPosition);
		
		audioSource = GetComponent<AudioSource>();		
	}
	
	void Start () {
		fired = 0;
		isSetted = false;
		AddHighlight();
	}
	
	public override void Update() {
		base.Update();
		if ( isSetted && (Time.time - fired) > model.rechargeTime ) {
			Shoot();
		}
	}
	

	
	private void Shoot() {
		fired = Time.time;
		foreach(Monster m in Scene.Instance.getMonsters()){
			if(Scene.Distance(m.position, position) <= attackRadius){
//				Debug.Log("Attack");
				AttackMonster(m, new ArrayList());		
			}
		}
		
	}
	
	private void AttackMonster(Monster monster, ArrayList affectedMonsters){
		
		audioSource.PlayOneShot(shootSound, 1f);
		
		Position sPos; 
		if (affectedMonsters.Count < 1) {
			sPos = model.position;
		} else {
			sPos = ((Monster)affectedMonsters[affectedMonsters.Count - 1]).position;
		}
		
		GameObject sparkObject = (GameObject)Instantiate(electricProjectile);
		sparkObject.transform.parent = gameObject.transform.parent;
		sparkObject.transform.localPosition = new Vector3(sPos.x * Scene.XRatio() + Scene.XRatio() / 2,sPos.y * Scene.YRatio() + Scene.YRatio() / 2,-4);
		ElectricSparkBehaviour spark = sparkObject.GetComponent<ElectricSparkBehaviour>();
		Projectile sparkModel = new Projectile(700, model.damage);
		spark.SetModel(sparkModel, sPos, monster);
		
		affectedMonsters.Add(monster);
		
		if (affectedMonsters.Count < 5) return;
		
		foreach(Monster m in Scene.Instance.getMonsters()){
			if(!affectedMonsters.Contains(m) && Scene.Distance(m.position, monster.position) <= attackRadius){
				ArrayList parameters = new ArrayList();
				parameters.Add(m);
				parameters.Add(affectedMonsters); 
				StartCoroutine(AttackWithDelay(parameters));
			}
		}
	}
	
	IEnumerator AttackWithDelay(ArrayList parameters){
		yield return new WaitForSeconds(chainDelay);
		AttackMonster((Monster)parameters[0], (ArrayList)parameters[1]);	
	}
	
	public void TowerSetted(){
		model.position = Scene.PositionFromCoordinate(transform.localPosition);
		isSetted = Scene.Instance.PlaceTower(model);
		
		if (isSetted) {
			GetComponent<BoxCollider>().enabled = false;
			position = Scene.PositionFromCoordinate(transform.localPosition);
			RemoveHighlight();
		} else {
			Destroy(gameObject);
		}
		
	}
	
}
