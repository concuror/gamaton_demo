using UnityEngine;
using System.Collections;

public abstract class AbstractTower {
	public int level = 1;
	public bool isDead = false;
	protected int currentLife;
	public abstract int life{
		get;
	}
	public abstract float rechargeTime{
		get;
	}
	public abstract int damage{
		get;
	}
	public Position aimedPosition;
	public Position position;
	public abstract int radius{
		get;
	}
	public bool active;
	
	public void AttackTower(int attackDamage) {
		this.currentLife -= attackDamage;
		if(currentLife <= 0) {
			Dead();
		}
	}
	
	private void Dead () {
		isDead = true;
	}
}
