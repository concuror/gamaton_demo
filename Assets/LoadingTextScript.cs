using UnityEngine;
using System.Collections;

public class LoadingTextScript : MonoBehaviour {
	
	private AudioSource audioSource;
	public AudioClip telegraph1;
	
	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource>();
		
		StartCoroutine("PlayTelegraph");
	}
	
	IEnumerator PlayTelegraph()
	{	
		for(int i=0; i<11; i++){		
			audioSource.PlayOneShot(telegraph1, 1);
			yield return new WaitForSeconds(0.41f);
		}
	}
	
	
}
